# 介绍
护卫盾核心库,护卫盾全功能库,内含登录窗口、注册窗口、改密窗口、卡密充值窗口、扫码充值窗口，使用此库，将极大提高开发效率，开发者仅需关注程序自身功能，繁琐重复的接入工作由护卫盾完成。

# 目录
`Bin/windows-x86`:windows-32位核心库

`Bin/windows-x86`:windows-64位核心库

`Bin/osx-arm64`:macos-arm64核心库

`Bin/osx-x64`:macos-amd64位核心库

`Bin/linux-x86`:linux-32位核心库

`Bin/linux-x64`:linux-64位核心库

`Bin/linux-arm32`:linux-arm32核心库

`Bin/linux-arm64`:linux-arm64核心库

`Include/C`:C/C++ 头文件

`Lib/C`:C/C++ 导入库


# 使用方法
* 首先下载对应的核心库
* C/C++可直接引入头文件,将库文件复制到程序输出目录即可
* C语言还需要额外引入:
    ```
    #include <stdbool.h>
    #include <stdlib.h>
    ```
* 其它不能使用C头文件的语言请前往 <a href="https://www.huweidun.cn/doc-view-10001.htm" target="_blank">护卫盾官网</a> 快速复制库函数声明
* 当前支持自动生成声明的语言有：易语言、C#、VB.NET、Python、GoLang等，更多语言适配中...
* 函数文档也可前往上述地址查看子选项。

# 快速接入
* 最快仅需三行代码即可完成接入
    ```
  hwd_config(false,true,true);// 置配置
  
  hwd_newLine();// 添加线路
  
  hwd_loadUI("login" ,"" ,800 ,700 ,3 ,false ,"123456");
  ```
