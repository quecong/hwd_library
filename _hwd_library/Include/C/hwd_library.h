﻿#ifndef HWD_LIBRARY_H
#define HWD_LIBRARY_H


#ifdef _WIN32
#ifdef HUWEIDUN_EXPORTS
#define HWD_LIBRARY_API __declspec(dllexport)
#else
#define HWD_LIBRARY_API __declspec(dllimport)
#endif
#elif defined(__APPLE__)
#define HWD_LIBRARY_API __attribute__((visibility("default")))
#elif defined(__linux__)
#define HWD_LIBRARY_API __attribute__((visibility("default")))
#else
#define HWD_LIBRARY_API
#endif

/// <summary>
/// 心跳回调函数模板,可用于接收心跳回调,设置回调函数后,心跳状态异常时,不会提示和退出,请在回调函数中自行处理.
/// </summary>
/// <param name="code">心跳状态码,200为正常,非200为异常</param>
/// <param name="message">心跳提示文本</param>
/// <returns></returns>
typedef void * (*HeartbeatCallback)(int code, const char *message);

#ifdef __cplusplus
extern "C" {
#endif

/// <summary>
/// 设置全局返回字符串的编码,不支持宽字符,如不调用此函数或参数设置为空,则默认返回字符串为UTF-8编码,如调用此函数,必须在调用含有字符串返回值的函数之前调用本函数,否则不会自动转码.
/// </summary>
/// <param name="encoding">编码类型,可使用hwd_detectEncoding检查你使用语言的字符串编码,省去转码步骤</param>
/// <returns>返回字符编码类型</returns>
HWD_LIBRARY_API void hwd_setResultEncoding(const char *encoding);

/// <summary>
/// 设置启动参数,如不调用此函数,则默认proCom=false,checkDebugger=true,checkVirtualMachine=true,loginRouterCheckLoginState=true,heartbeatCallback=nullptr
/// </summary>
/// <param name="proCom">是否启用进程通信,默认为false,如果为true,则开辟5M共享内存用于进程通信,本进程或其他进程可使用hwd_getPcMsg()函数读取共享资料,具体参照hwd_getPcMsg()参数说明.</param>
/// <param name="checkDebugger">是否检测调试器,默认为true,开发阶段必须为false,否则无法调试,发布时一定设置为true</param>
/// <param name="checkVirtualMachine">是否检测虚拟机,默认为true,根据自身情况设置,如为true,则无法在虚拟机中运行</param>
/// <param name="loginRouterCheckLoginState">内置UI登录窗口关闭时,是否校验登录状态,默认为true,如果为true,那么内置登录窗口关闭时,强制校验是否登录,如未登录,则退出整体进程,不在向后继续执行.</param>
/// <param name="heartbeatCallback">心跳回调函数指针,如果设置心跳回调函数,则心跳状态异常时不会提示和退出(例如用户到期,被顶下线,被封停等),回调函数传递两个参数,int code=心跳状态码,const char* message=心跳状态提示内容,如不需要,可传递null或0,默认为null</param>
/// <returns></returns>
HWD_LIBRARY_API void hwd_config(bool proCom, bool checkDebugger, bool checkVirtualMachine, bool loginRouterCheckLoginState, HeartbeatCallback heartbeatCallback);

/// <summary>
/// 添加线路,必须最先执行,如有多条线路,可重复调用此函数.
/// </summary>
/// <param name="name">线路名称,例如：线路一</param>
/// <param name="url">API地址,添加软件后在软件列表获取</param>
/// <param name="webkey">购买授权后,在我的授权中获取</param>
/// <param name="sid">软件ID,添加软件时设置</param>
/// <param name="key">通信秘钥,添加软件时设置</param>
/// <param name="clientRule">客户端规则,添加软件时设置</param>
/// <param name="serverRule">服务端规则,添加软件时设置</param>
/// <param name="clientVersion">客户端版本号</param>
/// <returns></returns>
HWD_LIBRARY_API void hwd_newLine(const char *name, const char *url, const char *webkey, const char *sid, const char *key, const char *clientRule, const char *serverRule, const char *clientVersion);

/// <summary>
/// 软件初始化.
/// </summary>
/// <param name="index">线路序号,通过hwd_newLine()添加线路,从0开始,例：如有多条线路,则顺序为：0,1,2,3...n</param>
/// <returns>成功返回true,失败返回false</returns>
HWD_LIBRARY_API bool hwd_init(int index);

/// <summary>
/// 根据提交参数名,返回网页端设置的软件数据管理器中对应的版本信息,例如更新包地址、更新后版本号等.
/// </summary>
/// <param name="version">客户端版本号,对应版本管理器中的“适用版本”</param>
/// <param name="name">newVer=更新后版本号,updateUrl=更新包地址,completeUrl=完整包下载地址,updateUrlX64=更新包X64,completeUrlX64=完整包X64,updateUrlARM=更新包ARM,completeUrlARM=完整包ARM,forceUpdate=是否强制更新(yes/no),visible=前台是否可见(yes/no),command=更新前后执行命令</param>
/// <param name="buffer">返回值缓冲区</param>
/// <param name="bufferLen">缓冲区尺寸</param>
/// <returns></returns>
HWD_LIBRARY_API void hwd_getSoftVersionInfo(const char *version, const char *name, char *buffer, size_t bufferLen);

/// <summary>
/// 根据提交参数名,返回网页端设置的软件数据,例如软件名、客户端公告等.
/// </summary>
/// <param name="name">name=软件名,versioninfo=版本管理器中所有数据(json格式由旧到新排列),version=服务端最新版本号,clientVersion=客户端版本号,heartbeattime=心跳时间,notice=客户端公告,qq=客服qq,website=官网地址,loginimg=登录页面图片,clientip=客户端IP地址,deduct=转绑扣除数量,login=登录方式(0:账号密码登录,1:充值卡登录),type=计费模式(0:计时,1:计点),retBind=注册绑定(1:手机-不验证,2:手机-短信验证,3:邮箱-不验证,4:邮箱-邮件验证),para=软件自定义常量(注意,只有登录成功才能取到此值.),captcha=需要验证码的位置(如此值包含 captcha_login 需要登录验证码,包含 captcha_recharge 充值验证码,包含 captcha_reg 注册验证码,包含 captcha_repwd 改密验证码[同时包含发送安全码和修改密码]))</param>
/// <param name="buffer">返回值缓冲区</param>
/// <param name="bufferLen">缓冲区尺寸</param>
/// <returns></returns>
HWD_LIBRARY_API void hwd_getSoftInfo(const char *name, char *buffer, size_t bufferLen);

/// <summary>
/// 根据提交参数,返回软件自定义常量中指定节点的值,只有用户正常登陆,才会返回此值,如果用户到期,且"允许到期登陆",那么也会返回此值(也属于正常登陆).注意,如使用此命令,必须保证软件自定义常量为标准JSON格式.
/// </summary>
/// <param name="name">属性名称,例 : 软件自定义常量为 {"提交地址":"xxx.com","version":"1.0"},则 : hwd_getSoftPara("提交地址"); 返回:xxx.com</param>
/// <param name="buffer">返回值缓冲区</param>
/// <param name="bufferLen">缓冲区尺寸</param>
/// <returns></returns>
HWD_LIBRARY_API void hwd_getSoftPara(const char *name, char *buffer, size_t bufferLen);

/// <summary>
/// 获取当前软件的充值卡列表信息,如卡类过多,注意调整缓冲区尺寸,否则将接收不全或跳异常.
/// </summary>
/// <param name="username">获取充值卡资料的用户名,账户如为超级会员,只返回当前超级会员可充值的卡类,如为软件会员,只返回当前软件会员可充值的卡类,否则为非会员,则返回全部可使用卡类（如有超级会员卡,sVIP参数可选是否返回超级会员卡）</param>
/// <param name="sVIP">是否返回超级会员充值卡,仅限非会员有效,如已是会员（指未到期或有点数）,此参数无效,自动按用户当前可使用卡类返回</param>
/// <param name="buffer">返回值缓冲区</param>
/// <param name="bufferLen">缓冲区尺寸</param>
/// <returns></returns>
HWD_LIBRARY_API void hwd_getCards(const char *username, bool sVIP, char *buffer, size_t bufferLen);

/// <summary>
/// 获取验证码-字节流（默认验证码需要预留 1024*10 的缓冲区）.
/// </summary>
/// <param name="buffer">返回值缓冲区</param>
/// <param name="bufferLen">缓冲区尺寸</param>
/// <returns></returns>
HWD_LIBRARY_API void hwd_getCaptchaImg(char *buffer, size_t bufferLen);

/// <summary>
/// 获取验证码-Base64格式字符串（默认验证码需要预留 1024*10 的缓冲区）.
/// </summary>
/// <param name="buffer">返回值缓冲区</param>
/// <param name="bufferLen">缓冲区尺寸</param>
/// <returns></returns>
HWD_LIBRARY_API void hwd_getCaptchaImgBase64(char *buffer, size_t bufferLen);

/// <summary>
/// 获取验证码-本地文件.
/// </summary>
/// <param name="buffer">返回值缓冲区</param>
/// <param name="bufferLen">缓冲区尺寸</param>
/// <returns></returns>
HWD_LIBRARY_API void hwd_getCaptchaImgFile(char *buffer, size_t bufferLen);

/// <summary>
/// 获取二维码订单号支付状态.
/// </summary>
/// <param name="orderId">订单号码,通过hwd_getQRCode()获得</param>
/// <returns>成功返回true,失败返回false</returns>
HWD_LIBRARY_API bool hwd_getOrderState(const char *orderId);

/// <summary>
/// 获取支付二维码.
/// </summary>
/// <param name="username">充值用户名</param>
/// <param name="cid">卡类ID,通过hwd_getCards()获得</param>
/// <param name="qrcode">如为true,则只返回支付链接(用于前端生成二维码),如为false,则由服务端生成png格式经过base64编码的二维码图片</param>
/// <param name="buffer">返回值缓冲区</param>
/// <param name="bufferLen">缓冲区尺寸</param>
/// <returns>成功返回json字符串,order_id:订单号(用于查询支付状态),qrcode_url:支付链接(可用于前端生成二维码),qrcode_png:经过base64编码的png格式的支付二维码图片,失败返回空</returns>
HWD_LIBRARY_API bool hwd_getQRCode(const char *username, int cid, bool qrcode, char *buffer, size_t bufferLen);

/// <summary>
/// 获取机器码.
/// </summary>
/// <param name="buffer">返回值缓冲区</param>
/// <param name="bufferLen">缓冲区尺寸</param>
/// <returns></returns>
HWD_LIBRARY_API void hwd_getMachineCode(char *buffer, size_t bufferLen);

/// <summary>
/// 注册通行证.
/// </summary>
/// <param name="username">注册用户名</param>
/// <param name="password">注册密码</param>
/// <param name="safeMode">邮箱或手机号码,根据后台配置（系统设置-验证设置-注册绑定）</param>
/// <param name="safeCode">邮箱或手机验证码（系统设置-验证设置-注册绑定）如要求验证,则必须填写,否则可空</param>
/// <param name="referrer">推荐人账号,可空</param>
/// <param name="code">验证码,如果 hwd_getSoftInfo("captcha") 中包含 "captcha_reg" , 则需要填写验证码,否则可留空.</param>
/// <returns>成功返回true,失败返回false</returns>
HWD_LIBRARY_API bool hwd_reg(const char *username, const char *password, const char *safeMode, const char *safeCode, const char *referrer, const char *code);

/// <summary>
/// 发送密码重置安全码.
/// </summary>
/// <param name="username">用户名</param>
/// <param name="flag">发送标记,reg=注册验证码,rePwd=改密验证码</param>
/// <param name="safeType">发送类型,mail=邮件验证码,phone=手机验证码</param>
/// <param name="safeMode">绑定邮箱或手机，如果safeType=mail,则为邮箱,如果safeType=phone,则为手机号码</param>
/// <param name="code">验证码,如果 hwd_getSoftInfo("captcha") 中包含 "captcha_repwd" , 则需要填写验证码,否则可留空.</param>
/// <returns>成功返回true,失败返回false</returns>
HWD_LIBRARY_API bool hwd_sendSafeCode(const char *username, const char *flag, const char *safeType, const char *safeMode, const char *code);

/// <summary>
/// 修改密码.
/// </summary>
/// <param name="username">用户名</param>
/// <param name="password">新密码</param>
/// <param name="safeCode">邮件或短信验证码</param>
/// <param name="code">验证码,如果 hwd_getSoftInfo("captcha") 中包含 "captcha_repwd" , 则需要填写验证码,否则可留空.</param>
/// <returns>成功返回true,失败返回false</returns>
HWD_LIBRARY_API bool hwd_resetPwd(const char *username, const char *password, const char *safeCode, const char *code);

/// <summary>
/// 充值卡充值.
/// </summary>
/// <param name="user">充值用户名</param>
/// <param name="cardnum">充值卡号</param>
/// <param name="code">验证码,如果 hwd_getSoftInfo("captcha") 中包含 "captcha_recharge" , 则需要填写验证码,否则可留空</param>
/// <returns>成功返回true,失败返回false</returns>
HWD_LIBRARY_API bool hwd_recharge(const char *user, const char *cardnum, const char *code);

/// <summary>
/// 添加黑名单.
/// </summary>
/// <param name="code">黑名单号码,可以是IP地址或机器码,IP地址:禁止一切访问(包括网站),机器码:禁止客户端访问(不包括网站,因为网站获取不到机器码,无法判断.)</param>
/// <param name="remark">添加黑名单理由</param>
/// <returns>成功返回true,失败返回false</returns>
HWD_LIBRARY_API bool hwd_addBlackList(const char *code, const char *remark);

/// <summary>
/// 用户登录.
/// </summary>
/// <param name="username">账号密码模式为登录账号,充值卡登录为卡号.</param>
/// <param name="password">账号密码模式为登录密码,充值卡登录无需填写.</param>
/// <param name="code">验证码,如果 hwd_getSoftInfo("captcha") 中包含 "captcha_login" , 则需要填写验证码,否则可留空.</param>
/// <param name="autoHeartbeat">登录成功是否自动心跳,心跳周期取后台软件设置中的心跳周期.</param>
/// <returns>成功返回true,失败返回false</returns>
HWD_LIBRARY_API bool hwd_login(const char *username, const char *password, const char *code, bool autoHeartbeat);

/// <summary>
/// 获取登录用户信息,根据提交参数名,返回指定用户数据.
/// </summary>
/// <param name="name">username=用户名,password=密码,token=登录token(用于校验登录状态),tokenWeb=网页端免密登录token,auth=登录令牌,endtime=到期时间,point=点数余额,balance=账户余额,para=用户自定义数据,bind=用户绑定信息,shareCode=分享码,shareUrl=分享链接</param>
/// <param name="buffer">返回值缓冲区</param>
/// <param name="bufferLen">缓冲区尺寸</param>
/// <returns></returns>
HWD_LIBRARY_API void hwd_getUserInfo(const char *name, char *buffer, size_t bufferLen);

/// <summary>
/// 根据提交参数,返回用户自定义常量中指定节点的值,只有用户正常登陆且未到期/有点数,才会返回此值.注意,如使用此命令,必须保证用户自定义常量为标准JSON格式.
/// </summary>
/// <param name="name">属性名称,例 : 用户自定义常量为 {"版本":"普通版","高级功能":"ON"},则 : hwd_getUserPara("版本"); 返回:普通版</param>
/// <param name="buffer">返回值缓冲区</param>
/// <param name="bufferLen">缓冲区尺寸</param>
/// <returns></returns>
HWD_LIBRARY_API void hwd_getUserPara(const char *name, char *buffer, size_t bufferLen);

/// <summary>
/// 扣点,计点模式可用.只有相同的point和remarks才会过滤,例如:hwd_deductPoint(1,"日费用",86400) 和 hwd_deductPoint(30,"月费用",2592000);这两个并不冲突,因为扣点数量和扣点备注均不同.
/// </summary>
/// <param name="point">扣除点数,最小为1点</param>
/// <param name="remarks">扣点备注,管理可在后台查看,用户可在个人中心查看(请在"软件编辑"中开启"记录扣点日志")</param>
/// <param name="interval">扣点间隔(单位:秒),0为不限,即每次都扣点.大于零代表指定间隔内不重复扣点,例如1天只扣一次点,那么间隔就是86400秒,需在软件后台开启:记录扣点日志().</param>
/// <returns>成功返回true,失败返回false</returns>
HWD_LIBRARY_API bool hwd_deductPoint(int point, const char *remarks, int interval);

/// <summary>
/// 扣时,计时模式可用.只有相同的minute和remarks才会过滤,例如:hwd_deductTime(1,"日费用",86400) 和 hwd_deductTime(30,"月费用",2592000);这两个并不冲突,因为扣时数量和扣时备注均不同.
/// </summary>
/// <param name="minute">扣除时间,单位:分钟,最小为1分钟</param>
/// <param name="remarks">扣时备注,管理可在后台查看,用户可在个人中心查看(请在"软件编辑"中开启"记录扣点日志")</param>
/// <param name="interval">扣时间隔(单位:秒),0为不限,即每次都扣时.大于零代表指定间隔内不重复扣时,例如1天只扣一次时,那么间隔就是86400秒,需在软件后台开启:记录扣点日志().</param>
/// <returns>成功返回true,失败返回false</returns>
HWD_LIBRARY_API bool hwd_deductTime(int minute, const char *remarks, int interval);

/// <summary>
/// 扣余额,登录模式为:"账号密码"时有效,只有相同的money和remarks才会过滤,例如:hwd_deductBalance(1,"日费用",86400) 和 hwd_deductBalance(33,"月费用",2592000);这两个并不冲突,因为扣除数量和扣除备注均不同.
/// </summary>
/// <param name="money">扣除金额,单位:元,最小为0.01元</param>
/// <param name="remarks">扣除备注,管理可在后台查看,用户可在个人中心查看(请在"软件编辑"中开启"记录扣点日志")</param>
/// <param name="interval">扣除间隔(单位:秒),0为不限,即每次都扣除.大于零代表指定间隔内不重复扣除,例如1天只扣一次余额,那么间隔就是86400秒,需在软件后台开启:记录扣点日志().</param>
/// <returns>成功返回true,失败返回false</returns>
HWD_LIBRARY_API bool hwd_deductBalance(double money, const char *remarks, int interval);

/// <summary>
/// 绑定用户资料,例如配置云备份,绑定游戏号等.用户登录成功状态下,可使用hwd_getUserInfo("bind")获取此绑定资料.
/// </summary>
/// <param name="str">欲写入的数据,理论无长度限制,由于数据加密传输,数据越长加密时间越慢,因此不建议数据太大.</param>
/// <returns>成功返回true,失败返回false</returns>
HWD_LIBRARY_API bool hwd_setUserBind(const char *str);

/// <summary>
/// 绑定机器码,自动将指定账户绑定本机,无需传入机器码,自动获取,如已达到绑定上限,则删除最先绑定的机器码.转绑扣时扣点自动完成,无需独立扣除.
/// </summary>
/// <param name="username">欲绑定的用户名,无需传入机器码,机器码自动获取.</param>
/// <returns>成功返回true,失败返回false</returns>
HWD_LIBRARY_API bool hwd_bindMachineCode(const char *username);

/// <summary>
/// 退出登录,程序退出前可调用此命令,服务端立即更新用户状态,否则需要等待无心跳通讯后,才能判定用户退出.
/// </summary>
/// <returns>成功返回true,失败返回false</returns>
HWD_LIBRARY_API bool hwd_logout();

/// <summary>
/// 动态调用自定义函数(PHP语法),预估返回数据尺寸,使用合适的缓冲区尺寸
/// </summary>
/// <param name="name">函数名,例如:function test($a,$b){return $a + $b},函数名为:test</param>
/// <param name="para">参数值,例如:function test($a,$b){return $a + $b},参数值为:"3,4" 参数分隔符为英文半角逗号(,)</param>
/// <param name="buffer">返回值缓冲区</param>
/// <param name="bufferLen">缓冲区尺寸</param>
/// <returns>成功返回true,失败返回false</returns>
HWD_LIBRARY_API bool hwd_callPHP(const char *name, const char *para, char *buffer, size_t bufferLen);

/// <summary>
/// 获取云端独立自定义常量.（时时联网获取,根据常量数据长度,使用合适的缓冲区尺寸）.
/// </summary>
/// <param name="flag">常量类型,0=软件独立自定义常量,1=用户独立自定义常量</param>
/// <param name="name">常量名</param>
/// <param name="buffer">返回值缓冲区</param>
/// <param name="bufferLen">缓冲区尺寸</param>
/// <returns></returns>
HWD_LIBRARY_API void hwd_getParam(int flag, const char *name, char *buffer, size_t bufferLen);

/// <summary>
/// 取主进程软件或用户资料,初始化时[hwd_init()]启用进程通讯后有效.
/// </summary>
/// <param name="key">通信秘钥,与初始化中的通信秘钥相同</param>
/// <param name="name">节点名:'soft.x'为软件数据(x格式同'hwd_getSoftInfo()'中'name'的值,例如soft.version),'user.x'为用户资料(x格式同'hwd_getUserInfo()'中'name'的值,例如user.endtime),'softpara.x'为取软件自定义常量节点值(x为节点名),'userpara.x'为取用户自定义常量节点值(x为节点名)</param>
/// <param name="buffer">返回值缓冲区</param>
/// <param name="bufferLen">缓冲区尺寸</param>
/// <returns></returns>
HWD_LIBRARY_API void hwd_getPcMsg(const char *key, const char *name, char *buffer, size_t bufferLen);

/// <summary>
/// 心跳包,保持与服务器通讯.请注意,此命令有两种功能,1.单次心跳,2.循环心跳,具体请看参数<1>说明.
/// </summary>
/// <param name="time">心跳周期,单位:秒,为0则单次心跳,若大于0,则最小120秒,最大不限,只要调用过1次循环心跳,则程序退出前均有效,若自动心跳,则此处心跳时间必须与后台软件设置中的"心跳时间"相同.即使使用自动心跳,也可单独调用hwd_heartbeat(0)进行单次心跳.</param>
/// <param name="loginAuth">登录令牌,主进程可留空,使用此令牌可在其它进程免登录心跳.</param>
/// <returns>成功返回true,失败返回false</returns>
HWD_LIBRARY_API bool hwd_heartbeat(int time, const char *loginAuth);


/// <summary>
/// 检测UI运行时是否存在,不存在则提示用户下载并退出程序
/// </summary>
/// <returns></returns>
HWD_LIBRARY_API void hwd_checkUiRuntime();

/// <summary>
/// 载入内置窗口,仅支持Windows,如为登录窗口,则登录成功后会自动启动心跳.如router留空,则使用url访问UI,使用router的时候,如首条线路宕机无法访问,则会顺序加载其它线路UI
/// </summary>
/// <param name="router">UI路由,权限高于url,可选值:login:登录页,resister:注册页,password:改密页,recharge:卡密充值,qrcode:扫码充值</param>
/// <param name="url">UI地址,二次开发使用,优先级低于router,如果router留空,则使用url作为ui地址</param>
/// <param name="width">窗口宽度,默认为800px</param>
/// <param name="height">窗口高度,默认为700px</param>
/// <param name="flag">窗口类型,默认为0,0:默认窗口（最大化、最小化、窗口可调整大小）,1:最小化按钮,2:最大化按钮,3:禁止调整大小,可使用位或运算符组合多个标识</param>
/// <param name="debug">是否允许浏览器内调试器,默认为false,开发皮肤时建议打开,发布时务必关闭</param>
/// <param name="menuItem">加载菜单项,仅载入登录页UI时需要填写,1:官方网站,2:注册账户,3:修改密码,4:卡密充值,5:扫码充值,6:客服QQ,可以组合使用,例如:"123456"或:"2356"</param>
/// <returns></returns>
HWD_LIBRARY_API void hwd_loadUI(const char *router, const char *url, int width, int height, int flag, bool debug, const char *menuItem);

/// <summary>
/// 通过系统默认浏览器载入内置窗口,如为登录窗口,则登录成功后会自动启动心跳.如router留空,则使用url访问UI,使用router的时候,如首条线路宕机无法访问,则会顺序加载其它线路UI
/// </summary>
/// <param name="router">UI路由,权限高于url,可选值:login:登录页,resister:注册页,password:改密页,recharge:卡密充值,qrcode:扫码充值</param>
/// <param name="url">UI地址,二次开发使用,优先级低于router,如果router留空,则使用url作为ui地址</param>
/// <param name="menuItem">加载菜单项,仅载入登录页UI时需要填写,1:官方网站,2:注册账户,3:修改密码,4:卡密充值,5:扫码充值,6:客服QQ,可以组合使用,例如:"123456"或:"2356"</param>
/// <returns></returns>
HWD_LIBRARY_API void hwd_loadUIBrowser(const char *router, const char *url, const char *menuItem);

/// <summary>
/// 系统内置询问消息框,支持跨平台
/// </summary>
/// <param name="title">标题</param>
/// <param name="msg">内容</param>
/// <param name="button">操作按钮,0=是否钮,1=确认取消钮,2=确认钮</param>
/// <returns></returns>
HWD_LIBRARY_API bool hwd_messageBox(const char *title, const char *msg, int button);

/// <summary>
/// 倒计时消息框,使用webUI,仅支持Windows
/// </summary>
/// <param name="title">提示标题</param>
/// <param name="body">提示内容</param>
/// <param name="okButtonText">确认按钮标题,留空则不显示.例如:确认、确定、同意、是</param>
/// <param name="cancelButtonText">取消按钮标题,留空则不显示.例如:取消、不同意、否</param>
/// <param name="countdownTime">倒计时时间,单位:秒,为0则不启用倒计时</param>
/// <param name="defaultButton">倒计时结束后默认点击按钮索引,仅在倒计时时间大于0时生效,可取值:0:取消,1:确认</param>
/// <param name="width">窗口宽度</param>
/// <param name="height">窗口高度</param>
/// <returns>返回点击索引,0:点击取消,1:点击确定</returns>
HWD_LIBRARY_API int hwd_messageBoxA(const char *title, const char *body, const char *okButtonText, const char *cancelButtonText, int countdownTime, int defaultButton, int width, int height);

/// <summary>
/// 倒计时消息框,使用webUI,系统内置浏览器,支持跨平台
/// </summary>
/// <param name="title">提示标题</param>
/// <param name="body">提示内容</param>
/// <param name="okButtonText">确认按钮标题,留空则不显示.例如:确认、确定、同意、是</param>
/// <param name="cancelButtonText">取消按钮标题,留空则不显示.例如:取消、不同意、否</param>
/// <param name="countdownTime">倒计时时间,单位:秒,为0则不启用倒计时</param>
/// <param name="defaultButton">倒计时结束后默认点击按钮索引,仅在倒计时时间大于0时生效,可取值:0:取消,1:确认</param>
HWD_LIBRARY_API int hwd_messageBoxABrowser(const char *title, const char *body, const char *okButtonText, const char *cancelButtonText, int countdownTime, int defaultButton);

/// <summary>
/// 快速写配置,自动创建"程序目录\config.dat",保存程序所需配置,配合hwd_read(string name);读取.
/// </summary>
/// <param name="name">配置名称</param>
/// <param name="value">配置值</param>
/// <returns>成功返回true,失败返回false</returns>
HWD_LIBRARY_API bool hwd_save(const char *name, const char *value);

/// <summary>
/// 快速读配置,可读取hwd_save();函数写下的配置.
/// </summary>
/// <param name="name">配置名称</param>
/// <param name="defaultValue">默认返回值</param>
/// <param name="buffer">返回值缓冲区</param>
/// <param name="bufferLen">缓冲区尺寸</param>
/// <returns></returns>
HWD_LIBRARY_API void hwd_read(const char *name, const char *defaultValue, char *buffer, size_t bufferLen);

/// <summary>
/// 快速验证,作者临时接单,一条命令快速接入验证,数据安全的前提下,防止被骗软件.此命令只需运行一次,程序结束前每隔几分钟自动校验一次.
/// </summary>
/// <param name="index">线路序号,通过hwd_newLine()添加线路,从0开始,例：如有多条线路,则顺序为：0,1,2,3...n</param>
/// <param name="softPara">软件自定义常量, 只要用户未付款, 此处一定留空.如果用户付款, 请将此软件的自定义常量写到此处, 将不再联网验证.也可在软件调试时使用.</param>
/// <returns>成功返回true,失败返回false</returns>
HWD_LIBRARY_API bool hwd_fastCheck(int index, const char *softPara);

/// <summary>
/// 根据提交参数名,返回网页端设置的软件数据,例如软件名,版本号.
/// </summary>
/// <param name="name">para=软件自定义常量,clientip=客户端IP</param>
/// <param name="buffer">返回值缓冲区</param>
/// <param name="bufferLen">缓冲区尺寸</param>
/// <returns></returns>
HWD_LIBRARY_API void hwd_getFastInfo(const char *name, char *buffer, size_t bufferLen);

/// <summary>
/// 快速验证通过后,根据提交参数,返回快验自定义常量中指定节点的值,注意,如使用此命令,必须保证快验自定义常量为标准JSON格式,否则请使用hwd_getFastInfo(); 获取数据后自行处理.
/// </summary>
/// <param name="name">属性名称,例 : 软件自定义常量为 {"提交地址":"xxx.com","version":"1.0"},则 : hwd_getFastPara("提交地址"); 返回:xxx.com</param>
/// <param name="buffer">返回值缓冲区</param>
/// <param name="bufferLen">缓冲区尺寸</param>
/// <returns></returns>
HWD_LIBRARY_API void hwd_getFastPara(const char *name, char *buffer, size_t bufferLen);

/// <summary>
/// 获取文件MD5值.
/// </summary>
/// <param name="filename">获取MD5值完整文件路径</param>
/// <param name="buffer">返回值缓冲区</param>
/// <param name="bufferLen">缓冲区尺寸</param>
/// <returns></returns>
HWD_LIBRARY_API void hwd_getFileMD5(const char *filename, char *buffer, size_t bufferLen);

/// <summary>
/// 获取字符串MD5值.注意,不会将str参数进行转码,是字符串原始值的MD5.
/// </summary>
/// <param name="str">获取MD5值的字符串</param>
/// <param name="buffer">返回值缓冲区</param>
/// <param name="bufferLen">缓冲区尺寸</param>
/// <returns></returns>
HWD_LIBRARY_API void hwd_getStrMD5(const char *str, char *buffer, size_t bufferLen);

/// <summary>
/// 获取主进程的运行目录.
/// </summary>
/// <param name="buffer">返回值缓冲区</param>
/// <param name="bufferLen">缓冲区尺寸</param>
/// <returns></returns>
HWD_LIBRARY_API void hwd_getRunningPath(char *buffer, size_t bufferLen);

/// <summary>
/// 获取护卫盾模块的运行目录.
/// </summary>
/// <param name="buffer">返回值缓冲区</param>
/// <param name="bufferLen">缓冲区尺寸</param>
/// <returns></returns>
HWD_LIBRARY_API void hwd_getModulePath(char *buffer, size_t bufferLen);

/// <summary>
/// 获取最后一次出错信息.
/// </summary>
/// <param name="buffer">返回值缓冲区</param>
/// <param name="bufferLen">缓冲区尺寸</param>
/// <returns></returns>
HWD_LIBRARY_API void hwd_getLastErrorMsg(char *buffer, size_t bufferLen);

/// <summary>
/// 获取最后一次出错错误码.
/// </summary>
/// <returns>成功返回错误码,返回200为无错误</returns>
HWD_LIBRARY_API int hwd_getLastErrorCode();

/// <summary>
/// 任意编码字符串转UTF8编码.
/// </summary>
/// <param name="str">待转换的字符串</param>
/// <param name="buffer">缓冲区</param>
/// <param name="bufferLen">缓冲区尺寸</param>
/// <returns>返回UTF8字符串</returns>
HWD_LIBRARY_API void hwd_convertToUTF8(const char *str, char *buffer, size_t bufferLen);

/// <summary>
/// 任意编码字符串转ANSI编码.
/// </summary>
/// <param name="str">待转换的字符串</param>
/// <param name="buffer">缓冲区</param>
/// <param name="bufferLen">缓冲区尺寸</param>
/// <returns>返回ANSI字符串</returns>
HWD_LIBRARY_API void hwd_convertToANSI(const char *str, char *buffer, size_t bufferLen);

/// <summary>
/// 检测字符串编码
/// </summary>
/// <param name="str">待检测的字符串</param>
/// <param name="buffer">缓冲区</param>
/// <param name="bufferLen">缓冲区尺寸</param>
/// <returns></returns>
HWD_LIBRARY_API void hwd_detectEncoding(const char *str, char *buffer, size_t bufferLen);

#ifdef __cplusplus
}
#endif


#endif //HWD_LIBRARY_H
